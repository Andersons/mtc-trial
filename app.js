(function() {
    "use strict";

    new Swiper('.hero-swiper', {
        speed: 600,
        loop: true,
        autoplay: {
          delay: 3500,
          disableOnInteraction: false
        },
        slidesPerView: 'auto',
        pagination: {
          el: '.swiper-pagination',
          type: 'bullets',
          clickable: true
        },
        effect: 'fade',
    
      });

      new Swiper('.swiper', {
        speed: 600,
        loop: true,
        slidesPerView: 'auto',
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },

      });

      new SimpleLightbox('.gallery a', { /* options */ });

      const siteHeader = document.querySelector( '.site-header' );

      window.onscroll = function() {
        if (window.pageYOffset > 10) {
          siteHeader.classList.add("sticky");
        } else {
          siteHeader.classList.remove("sticky");
        }
      };

      const menuToggle = document.querySelector('.menu-toggle');
      const offCanvasMenu = document.querySelector('.off-canvas-menu');

      const toggleMenu = () => {
          menuToggle.classList.toggle('is-open');
          offCanvasMenu.classList.toggle('show');
          document.getElementsByTagName('html')[0].classList.toggle('fixed');   
      }

      if (menuToggle && offCanvasMenu){
        menuToggle.addEventListener('click', function(){
          toggleMenu();
        });
        //close menu if on link click
        offCanvasMenu.addEventListener('click', function(e){
          if ( e.target.classList.contains('link') ){
            toggleMenu();
          }
        })
      }
  
  })()